package dto;

public class Password {
	//Doy los atributos de persona
	private static int longitud;
	private static String contraseņa;
	
	//Hago todos los constructores
	public Password() {
		this.longitud = 0;
		this.contraseņa = " ";
	}
	
	public Password(int longitd) {
		this.longitud = longitud;
		this.contraseņa = generarContraseņa(longitd);
	}
	//Aqui para generar la contraseņa lo hago dando un numero aleatorio y pasandolo a su correspondiente en ascii
	public static String generarContraseņa(int longitud) {
		int num;
		char[] array = new char[longitud];
		for (int i = 0; i < longitud; i++) {
			num = (int) (Math.random() * (122 - 48 + 1) + 48);
			array[i] = (char) num;
		}
		return String.copyValueOf(array);
	}

	public String toString() {
		return "Password [longitud=" + longitud + ", contraseņa=" + contraseņa + "]";
	}
	
	public static boolean esFuerte() {
		//Hago las variables para guardar cuantos habra de cada uno
		int numeros = 0;
		int minusculas = 0;
		int mayusculas = 0;
		
		//Hago un for para recorrer la contraseņa y voy comprovando con su valor en ascii
		//para ver que es
		for (int i = 0; i < longitud; i++) {
			if (contraseņa.charAt(i)>=97 && contraseņa.charAt(i)<=122) {
				minusculas++;
			}else if (contraseņa.charAt(i)>=65 && contraseņa.charAt(i)<=90) {
				mayusculas++;
			}else {
				numeros++;
			}
		}
		
		//Hago el comprovante para mirar si la contraseņa es buena o no
		if (mayusculas > 2 && minusculas > 1 && numeros > 5) {
			return true;
		}else {
			return false;
		}
	}
}
