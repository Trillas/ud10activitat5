package vista;
import javax.swing.JOptionPane;

import dto.Password;
import exceptions.excepciones;

public class view {
	//El metodo que ejecuta todo el programa
	public static void crearContrase�as (){
        //Pregunto de que tama�o quiere el array
		System.out.println("De que tama�o quieres el array de contrase�as");
        int tama�o = excepciones.preguntar();
        
        //Creo el array de passwords y de comprovantes de contrase�a
        Password[] arrayContrase�as = new Password[tama�o];
        boolean[] arrayComprovante = new boolean[tama�o];
        
        //Pregunto de que longitud quiere las contrase�as
		System.out.println("De que tama�o quieres las contrase�as");
        int longitud = excepciones.preguntar();
        
        for (int i = 0; i < tama�o; i++) {
			arrayContrase�as[i] = new Password(longitud);
			arrayComprovante[i] = arrayContrase�as[i].esFuerte();
			System.out.println("La contrase�a " + arrayContrase�as[i] + " es buena: " + arrayComprovante[i]);
		}

	}
	
	//Un metodo para preguntar un n�mero
	public static int preguntarNumero() {
		int num;
		String textoNum = JOptionPane.showInputDialog(null, " ");
		num = Integer.parseInt(textoNum);
		
		return num;
	}
	
}
